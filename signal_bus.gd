extends Node

# Inspiration phase state
signal time_slot_advanced(time_slot: StringName)

# Writing phase state
signal note_selected(note_id: StringName)
signal note_unselected(note_id: StringName)

# Clickable note
signal note_clicked(note_id: StringName)
