extends Resource
class_name SpriteResource

@export var id: StringName
@export var position := Vector2.ZERO
@export var texture: Texture2D