extends Resource
class_name InspirationPhaseState

const TIME_SLOTS := ["MORNING", "AFTERNOON", "NIGHT"]
const ACTIONS_PER_TIME_SLOT := 2
const TOTAL_ACTIONS_PER_DAY := len(TIME_SLOTS) * ACTIONS_PER_TIME_SLOT

@export var actions_done := 0
@export var themes_gathered := {}   # Use dict keys as a set
@export var objects_interacted := {}    # Use dict keys as a set


func increment_actions_done() -> void:
    var prev_time_slot = TIME_SLOTS[actions_done]
    actions_done += 1

    if prev_time_slot != get_current_time_slot():
        advance_time_slot()


func get_current_time_slot() -> StringName:
    return TIME_SLOTS[actions_done % ACTIONS_PER_TIME_SLOT]


func advance_time_slot():
    SignalBus.time_slot_advanced.emit(get_current_time_slot())
