extends SpriteResource
class_name InspirationObject

const RES_PATH_TEMPLATE := "res://inspiration_phase/objects/%s.tres"

# ID is file name
@export var note_id: StringName


static func from_id(obj_id: StringName) -> InspirationObject:
	return load(RES_PATH_TEMPLATE % obj_id)