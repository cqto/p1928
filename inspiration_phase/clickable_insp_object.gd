extends ClickableSprite
class_name ClickableInspObject


func _init(inspiration_resource: InspirationObject):
	sprite_res = inspiration_resource


func on_start_hover():
	var tween = create_tween()
	tween.tween_property(self, "position", Vector2.UP * 10, 0.1).as_relative()


func on_stop_hover():
	var tween = create_tween()
	tween.tween_property(self, "position", original_position, 0.1)