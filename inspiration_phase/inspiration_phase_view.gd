extends Node2D
class_name InspirationPhaseView

@export var scene_objects := [] as Array[InspirationObject]


func _ready():
	generate_scene_object_nodes()


func generate_scene_object_nodes() -> void:
	for obj in scene_objects:
		# NOTE: connect signal?
		pass
