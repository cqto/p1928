extends TextureButton
class_name ClickableSprite

@export var sprite_res: SpriteResource
var original_position: Vector2


func _init(res: SpriteResource):
	sprite_res = res


func _ready():
	texture_normal = sprite_res.texture
	position = sprite_res.position
	original_position = position
	texture_click_mask = get_alpha_mask_from_texture(texture_normal)
	mouse_entered.connect(on_start_hover)
	mouse_exited.connect(on_stop_hover)


func _pressed():
	pass
	

func on_start_hover():
	pass


func on_stop_hover():
	pass


func get_alpha_mask_from_texture(texture: Texture2D):
	var mask = BitMap.new()
	mask.create_from_image_alpha(texture.get_image())
	return mask

