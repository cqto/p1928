class_name JsonLoader

static func get_data_from_file(file_path: String):
    """
    Returns data from a JSON file, or raises an exception if there's an error
    """
    assert(FileAccess.file_exists(file_path), "File path does not exist")

    var file := FileAccess.open(file_path, FileAccess.READ)
    var json_file := JSON.new()
    var parse_result := json_file.parse(file.get_as_text())

    if parse_result != OK:
        assert(false, "Error parsing file %s" % file_path)
        
    return json_file.get_data()

