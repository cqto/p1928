extends Control

@export var text_speed := 0.04

var dialogue: PackedStringArray
@onready var timer := $Box/Timer
@onready var text := $Box/Text

var phrase_num := 0
var finished := false


func _input(event):
	if event.is_action_pressed("select") and text.text != " ":
		get_viewport().set_input_as_handled()
		if finished:
			if phrase_num >= len(dialogue):
				phrase_num = 0
				visible = false
				text.bbcode_text = " "
			else:
				next_phrase()
		else:
			text.visible_characters = len(text.text)
			

func show_dialogue(dialogue_resource):
	visible = true
	timer.wait_time = text_speed
	timer.start()
	await timer.timeout
	dialogue = dialogue_resource.dialogue
	next_phrase()


func next_phrase():
	finished = false
	
	text.bbcode_text = dialogue[phrase_num]
	
	text.visible_characters = 0
	
	while text.visible_characters < len(text.text):
		text.visible_characters += 1
		timer.start()
		await timer.timeout
		
	finished = true
	phrase_num += 1
	return
