extends Resource
class_name ThemeFragmentMap

const map := {
	PoemTheme.NATURE: [PoemFragment.F1, PoemFragment.F4, PoemFragment.F5, PoemFragment.F11, PoemFragment.F13, PoemFragment.F17],
	PoemTheme.BANDIT: [PoemFragment.F1, PoemFragment.F7],
	PoemTheme.DAWN: [PoemFragment.F4],
	PoemTheme.VIOLENCE: [PoemFragment.F5, PoemFragment.F9, PoemFragment.F16],
	PoemTheme.LONGING: [PoemFragment.F2, PoemFragment.F3, PoemFragment.F6, PoemFragment.F7, PoemFragment.F8, PoemFragment.F9, PoemFragment.F10, PoemFragment.F11, PoemFragment.F14],
	PoemTheme.DEATH: [PoemFragment.F2, PoemFragment.F9, PoemFragment.F16],
	PoemTheme.LOSS: [PoemFragment.F8, PoemFragment.F10, PoemFragment.F12, PoemFragment.F14],
	PoemTheme.NIGHT: [PoemFragment.F12, PoemFragment.F15, PoemFragment.F16],
	PoemTheme.CRUCIFIX: [PoemFragment.F2],
	PoemTheme.BLOOD: [PoemFragment.F10],
	PoemTheme.DEW: [PoemFragment.F12],
	PoemTheme.ACAB: [PoemFragment.F16],
	PoemTheme.CALM: [PoemFragment.F17]
}


static func get_fragment_ids_from_themes(theme_list: Array):
	var fragment_ids = theme_list.map(func(theme): return map[theme]).reduce(
		func(acc: Array, fragments: Array):
			return fragments.filter(
				func(fragment): return acc.has(fragment)
			),
	)
	return fragment_ids if fragment_ids != null else []

static func get_fragments_from_themes(theme_list: Array):
	return ThemeFragmentMap.get_fragment_ids_from_themes(theme_list)\
		.map(func(frag_id): return PoemFragment.from_id(frag_id))
