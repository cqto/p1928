class_name PoemTheme

# General themes
const LONGING := &"longing"
const BANDIT := &"bandit"
const DEATH := &"death"
const DAWN := &"dawn"
const NATURE := &"nature"
const VIOLENCE := &"violence"
const LOSS := &"loss"
const NIGHT := &"night"

# Specific themes
const CRUCIFIX := &"crucifix"
const BLOOD := &"blood"
const DEW := &"dew"
const ACAB := &"acab"
const CALM := &"calm"
