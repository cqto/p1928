extends Resource
class_name PoemFragment

const PATH_TEMPLATE := "res://poem/fragments/%s.tres"
const PREVIEW_LENGTH := 12

const F1 := &"fragment_1"
const F2 := &"fragment_2"
const F3 := &"fragment_3"
const F4 := &"fragment_4"
const F5 := &"fragment_5"
const F6 := &"fragment_6"
const F7 := &"fragment_7"
const F8 := &"fragment_8"
const F9 := &"fragment_9"
const F10 := &"fragment_10"
const F11 := &"fragment_11"
const F12 := &"fragment_12"
const F13 := &"fragment_13"
const F14 := &"fragment_14"
const F15 := &"fragment_15"
const F16 := &"fragment_16"
const F17 := &"fragment_17"


@export var id: StringName
@export var themes: Array[StringName]
@export var verse_codes: Array[StringName]


func get_verses():
    return verse_codes.map(tr)


func get_preview():
    return tr(verse_codes[0]).substr(0, PREVIEW_LENGTH).trim_suffix(" ") + "..."


static func from_id(frag_id: StringName) -> PoemFragment:
    return load(PATH_TEMPLATE % frag_id)