extends ClickableSprite
class_name ClickableNote

var selected := false:
	# TODO: set definitive selected visuals
	set(new_val):
		self_modulate = Color(0, 0, 0.5, 1) if new_val else Color(1, 1, 1, 1) 


func _init(res: SpriteResource):
	super(res)


func _ready():
	super()
	SignalBus.note_selected.connect(set_selected)
	SignalBus.note_unselected.connect(set_unselected)


func _pressed():
	SignalBus.note_clicked.emit(sprite_res.id)


func on_start_hover():
	pass


func on_stop_hover():
	pass


func set_selected(note_id: StringName):
	if note_id == sprite_res.id:
		selected = true


func set_unselected(note_id: StringName):
	if note_id == sprite_res.id:
		selected = false