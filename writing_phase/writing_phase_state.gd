extends Resource
class_name WritingPhaseState

const MAX_WRITTEN_FRAGMENTS_PER_DAY := 5

@export var selected_note_ids: Array[StringName] = []
@export var written_fragment_ids: Array[StringName] = []


func toggle_selection(note_id: StringName):
	if not selected_note_ids.has(note_id):
		add_to_selected(note_id)
	else:
		remove_from_selected(note_id)


func add_to_selected(note_id: StringName):
	selected_note_ids.append(note_id)
	SignalBus.note_selected.emit(note_id)
	
	
func remove_from_selected(note_id: StringName):
	selected_note_ids.erase(note_id)
	SignalBus.note_unselected.emit(note_id)


func get_available_fragments_from_selected_notes():
	var theme_list := selected_note_ids.map(func(note_id): return Note.from_id(note_id).theme)
	return ThemeFragmentMap.get_fragments_from_themes(theme_list)
