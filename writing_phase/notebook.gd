extends Resource
class_name Notebook

const MID_LINE = Rect2i(684, 219, 109, 704)
const TOP_BORDER = 220
const LEFT_BORDER = 160
const BOTTOM_BORDER = 920
const RIGHT_BORDER = 1320

@export var notes: Array[Note]


static func get_positioned_notes_without_collisions(note_list: Array[Note]) -> Array[Note]:
	return note_list.reduce(Notebook.add_without_collisions, [] as Array[Note])


static func add_without_collisions(acc: Array[Note], new: Note) -> Array[Note]:
	var avaliable = false
	var posx
	var posy
	var failed
	var new_rect
	while(avaliable == false):
		posx = randi_range(LEFT_BORDER, RIGHT_BORDER-new.texture.get_width())	#"note.centerd = false" when adding child
		posy = randi_range(TOP_BORDER, BOTTOM_BORDER-new.texture.get_height())	#do not forget
		new_rect = Rect2i(posx, posy, new.texture.get_width(), new.texture.get_height())
		failed = false
		for rec in acc.map(Notebook.get_rect_from_note) + [MID_LINE]: 
			if(new_rect.intersects(rec)):
				failed = true
				break
		if(failed == false):
			avaliable = true
	new.position = Vector2(posx, posy)
	acc.append(new)
	return acc


static func get_rect_from_note(note: Note) -> Rect2i:
	return Rect2i(note.position.x as int, note.position.y as int, note.texture.get_width(), note.texture.get_height())
