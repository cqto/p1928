extends SpriteResource
class_name Note

const RES_PATH_TEMPLATE := "res://writing_phase/notes/%s.tres"

# ID is file name
@export var theme: StringName	# PoemTheme


static func from_id(obj_id: StringName) -> Note:
	return load(RES_PATH_TEMPLATE % obj_id)
