extends Node2D

@export var state: WritingPhaseState
@export var notes: Array[Note]


func _ready():

	# TODO: set notes before loading scene
	notes = load_notes_demo()

	notes = Notebook.get_positioned_notes_without_collisions(notes)
	state = WritingPhaseState.new()
	SignalBus.note_clicked.connect(state.toggle_selection)
	generate_note_nodes()

	# TODO: improve when actual code is in place
	SignalBus.note_selected.connect(show_fragments_from_selected_notes_demo.unbind(1))
	SignalBus.note_unselected.connect(show_fragments_from_selected_notes_demo.unbind(1))


func generate_note_nodes() -> void:
	for note in notes:
		add_child(ClickableNote.new(note))


# TODO: remove demo code
func load_notes_demo() -> Array[Note]:
	return [Note.from_id("lamp_ph"), Note.from_id("lamp_ph2")]


# TODO: remove demo code
func show_fragments_from_selected_notes_demo():
	var fragments = state.get_available_fragments_from_selected_notes()
	
	print("\n--Current fragments--")
	for fragment in fragments:
		print(fragment.get_preview())
